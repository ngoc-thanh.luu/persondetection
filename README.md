# Person Detection

Detect people in the direction of learning from above

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.

```bash
pip install -r requirements.txt
#intall torch cuda-11
pip3 install torch torchvision torchaudio --extra-index-url https://download.pytorch.org/whl/cu113
```

## Download weights

```bash
Drive: https://drive.google.com/drive/folders/1Nbdk19Txyo1A3cjtzPjyAO9Xw1gSLTXB?usp=sharing
```

## Usage

```python
pythonn detect.py --weight weigths/best.pt --source images/something.jpg
```

## Dataset train

CVat, WiderPerson, CrowdHuman, VisDrone, Background COCO

Source Dataset: /thanhln/mnt/source_dataset

Dataset Training: Vis_Cro_Wider_Total ( included dataset cvat, wider, crowd, visdrone, background) 

+ Number train: 42392 image
+ Object size into train: Small (211057); Med (280458); Large (194356)
+ Number val: 9135 image
+ Object size into val: Small (39638); Med (61467); Large (51494)
+ Number background: 1455 image 

Note: Small (size < 32*32); Med (32 * 32,96 * 96); Large(>96 * 96).

## Training 
bash train_exp16.sh

## Eval error
Link sheet: https://docs.google.com/spreadsheets/d/11QY7-69pD6z-5_7WoFY7lOykdqvDZw5KXsmAvAevg0k/edit?usp=sharing

## Dataset test
Link drive: https://drive.google.com/drive/folders/1XpWeogqxKMt7Yw5gW91BKhocv4iwywmK?usp=sharing 

## Plan training before
sheet "thu nghiem lan 2" removed ignore region in dataset crowd human dataset and wider person dataset

Link sheet: https://docs.google.com/spreadsheets/d/1uwO2IfIT9Yr8mJg6XcmuccFRDli-4pFIAyeh10PcTlo/edit?usp=sharing