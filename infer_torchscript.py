import argparse
from dataclasses import dataclass
import enum
import os
from statistics import mode
import sys
from pathlib import Path

import torch
import torch.backends.cudnn as cudnn
FILE = Path(__file__).resolve()
ROOT = FILE.parents[0]  # YOLOv5 root directory
if str(ROOT) not in sys.path:
    sys.path.append(str(ROOT))  # add ROOT to PATH
ROOT = Path(os.path.relpath(ROOT, Path.cwd()))  #

from models.common import DetectMultiBackend
from utils.datasets import IMG_FORMATS, VID_FORMATS, LoadImages, LoadStreams
from utils.general import (LOGGER, check_file, check_img_size, check_imshow, check_requirements, colorstr, cv2,
                           increment_path, non_max_suppression, print_args, scale_coords, strip_optimizer, xyxy2xywh)
from utils.plots import Annotator, colors, save_one_box
from utils.torch_utils import select_device, time_sync
from utils.augmentations import letterbox
import numpy as np 

weights = "weights/yolov5s.torchscript"
imgsz=(640,640)
conf_thres=0.25
iou_thres=0.45 # NMS IOU threshold
max_det=1000  # maximum detections per image

nosave=False  # do not save images/videos
classes=None  # filter by class: --class 0, or --class 0 2 3
agnostic_nms=False  # class-agnostic NMS
augment=False  # augmented inference
visualize=False  # visualize features
update=False
half=False  # use FP16 half-precision inference
dnn=False # use OpenCV DNN for ONNX inference
data = "data/coco128.yaml"

device  = select_device("2")
model = DetectMultiBackend(weights=weights, device=device, dnn=dnn, data=data,fp16=True)
stride, names, pt = model.stride, model.names, model.pt
imgsz = check_img_size(imgsz, s=stride)


@torch.no_grad()
def infer(imgs):
    bs = len(imgs)
    img0s = [np.array(im) for im in imgs]
    img = []
    print("Batch size:", bs )
    if bs > 1:
        s = np.stack([letterbox(x, imgsz, stride=stride, auto=True)[0].shape for x in imgs])
        rect = np.unique(s, axis=0).shape[0] == 1  # rect inference if all shapes equal
        img = [letterbox(x, imgsz, stride=stride, auto=rect and True)[0] for x in imgs]
        img = np.stack(img, 0)

        # Convert
        img = img[..., ::-1].transpose((0, 3, 1, 2))  # BGR to RGB, BHWC to BCHW
        img = np.ascontiguousarray(img)
        
    else:
        # Padded resize
        img = letterbox(imgs[0], imgsz, stride=stride, auto=False)[0]

        # Convert
        img = img.transpose((2, 0, 1))[::-1]  # HWC to CHW, BGR to RGB
        img = np.ascontiguousarray(img)
    print("SHAPE IMAGE: ",img.shape)
    model.warmup(imgsz=(1 if pt else bs, 3, *imgsz))

    im = torch.from_numpy(img).to(device)
    im = im.half() if model.fp16 else im.float()
    im /= 255
    if len(img.shape) == 3:
        im = im[None] 
    pred = model(im, augment=augment, visualize=visualize)
    pred = non_max_suppression(pred, conf_thres, iou_thres, classes, agnostic_nms, max_det=max_det)

    for i, det in enumerate(pred):
        image = img0s[i]
        if len(det):
            # gn = torch.tensor(image.shape)[[1, 0, 1, 0]]
            det[:, :4] = scale_coords(im.shape[2:], det[:, :4], image.shape).round()
            # print(det)
            for *xyxy, conf, cls in reversed(det):
                # xywh = (xyxy2xywh(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist()
                # print(xyxy.tolist())
                xyxy = [int(i.tolist()) for i in xyxy]
                score = round(float(conf.tolist()),2)
                cls = int(cls)
                if cls == 0:
                    cv2.rectangle(image, (xyxy[0], xyxy[1]), (xyxy[2], xyxy[3]), (255,2,22), 3)
                
        cv2.imwrite("some_img{}.jpg".format(i), image)
                
                



if __name__=="__main__":
    from glob import glob 
    from tqdm import tqdm 
    import cv2 
    import time 
    pth_imgs = glob("/workspace/dev/thanhln/Projects/processing_dataset/crowdhuman/images/val/*")
    for i, pth_img in enumerate(pth_imgs):
        
        # img = [cv2.imread(pth_imgs[i]), cv2.imread(pth_imgs[i+1])]
        img = [cv2.imread(pth_imgs[i])]
        t1 = time.time()
        infer(img)
        print("time infer:", time.time()-t1)
        # break