taskset -c 10-30 nohup python -m torch.distributed.launch \
--nproc_per_node 2 train.py --batch 128 --data data/exp16.yaml \
--weights '' --device 1,2 \
--epochs 250 \
--imgsz 640 \
--hyp data/hyps/person-hyp-med.yaml \
--cfg models/yolov5m.yaml